import React, { Component } from 'react';

import CheckboxLabel from './components/checkbox-label/CheckboxLabel';


class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      isChecked: false
    };
  }
  handeCheckChange (event) {
    this.setState({
      isChecked: event.value
    });
  }
  render() {
    return (
      <div className="App" aria-label="my test">
        <CheckboxLabel checked={this.state.isChecked} onCheckChange={this.handeCheckChange.bind(this)}>label content</CheckboxLabel>
      </div>
    );
  }
}

export default App;
