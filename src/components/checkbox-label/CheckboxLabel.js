import React, { Component } from 'react';
import './CheckboxLabel.css';
import PropTypes from 'prop-types';


class CheckboxLabel extends Component {
    handleClick (proxy, event) {
        if (this.props.onCheckChange) {
            this.props.onCheckChange({
                target: this,
                originalEvent: event,
                value: !this.props.checked
            });
        }
    }
    render() {
        return (
        <label className="checkbox-label" aria-label="checkbox" onClick={this.handleClick.bind(this)}>
            <span className="checkbox-label__checkbox">{this.props.checked && 'X'}</span>
            <span className="checkbox-label__label">{this.props.children}</span>
        </label>
        );
    }
}

CheckboxLabel.propTypes = {
    onCheckChange: PropTypes.func,
    checked: PropTypes.bool
};

export default CheckboxLabel;
