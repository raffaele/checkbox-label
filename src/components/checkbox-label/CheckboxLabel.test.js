import React from 'react';
import ReactDOM from 'react-dom';
import CheckboxLabel from './CheckboxLabel';

describe('CheckboxLabel component', () => {
    it('should renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<CheckboxLabel />, div);
    });

    xit('should call the related callback by passing the correct value', () => {
        const div = document.createElement('div');
        let receivedEvt;
        function handeCheckChange (evt) {
            receivedEvt = evt;
        }
        let result = ReactDOM.render(<CheckboxLabel checked={false} onCheckChange={handeCheckChange} />, div);

        expect(receivedEvt.value).toBe(true);
    });
});

