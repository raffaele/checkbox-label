This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

I'm preparing this file after the end of 30 minutes (the rest of the project was implemented in the 30 minutes specified in the request).

The component called `CheckboxLabel` is created to emulate a real checkbox behaviour.

Usage:

```
<CheckboxLabel checked={boolean_is_it_checked} onCheckChange={valueChangeHandlerMethod}>
    text for label
</CheckboxLabel>
```

List of params:

1. `checked` (boolean) - if true the checkbox is checked
2. `onCheckChange` (method) - called whenever the component is clicked, the method is called with an object with 3 attributes
    1. `target`: a reference to the component
    2. `originalEvent`: the click-event
    3. `value`: the new value that the checkbox should have
3. The children of the component will be used as per label.

I tried also to add some unit test, but I had to give up for the time was expired.
